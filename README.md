# Fearsome Frontier #

As a [Ludum Dare 36 entry](http://ludumdare.com/compo/ludum-dare-36/?action=preview&uid=113330), Fearsome Frontier is a simple tower defense game with the goal of protecting a treasure chest against raiders.

The player would place traps, start the game and activate the traps one by one.

Although the game is far from finished, it was fun to develop. A lot of content and features did not make it into the game, due to the lack of time for additional testing and debugging.

## Changelog ##

| Version | Date       | Changes                                                               |
| ------- | ---------- | --------------------------------------------------------------------- |
| v2.0.6  | 2024-08-18 | Level of difficulty; Reset camera on start; Lamp shadows              |
| v2.0.5  | 2024-08-11 | Improve orientation of lamps and chests                               |
| v2.0.4  | 2024-08-10 | Fix robber home teleport; Increase fall speed                         |
| v2.0.3  | 2024-08-10 | Improve robber controller; Tags for selector raycasts                 |
| v2.0.2  | 2024-08-09 | Camera orbit around clickpoint; Fix robber fall                       |
| v2.0.1  | 2024-08-07 | Larger rooms, fix lamps; Propagate tile surroundings                  |
| v2.0.0  | 2024-08-04 | Patches for Unity 2022.3                                              |
| v1.0    | 2016-10-08 | Original                                                              |

## Setup ##

Unity packages which Fearsome Frontier depends on:

1. Unity UI
2. Mathematics

## ToDo List ##

1. Fix entrance generation.
2. Animate the treasure chest.
3. Improve on the progression of level of difficulty.
4. Robbers sometimes start walking through walls after bumping into others while scared.

### Map generation ###

The map generation is heavily based on a [reddit post of phidinh6](https://www.reddit.com/r/gamedev/comments/1dlwc4/procedural_dungeon_generation_algorithm_explained/). Map generation procedure:

1. Room rectangles (coordinates, width and height) are generated with Park-Miller's RNG.

2. The resulting rectangles are checked against map edges.

3. Spanning tree and Delaunay triangulation of the room centres are generated.

4. The spanning tree is used for generating corridors between the rooms.

5. With a 50% probability, additional corridors are produced based on the edges from Delaunay triangulation.

6. The rectangles and L-shaped corridors are drawn on a map array.

7. Room tiles are drawn with a rim of walls around each pixel, unless some of the tiles are already floor tiles.
 

### Team ###
1. Ave Kodar - 2D, 3D art
2. Ehar Kala - sound, music, 2D art
3. Indrek Sünter - programmer
4. Mart Rätsep - 3D art
5. Reimo Luik - programmer, 2D art