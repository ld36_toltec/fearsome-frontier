﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
	public enum state_type {
		Menu = 0,
		PlacingTraps,
		Running,
		GameOver,
		RoundWon
	};

	public Text countdownText;
	private state_type state;
	private AudioSource audio_src;
	public AudioClip sound_game_lost;
	public AudioClip sound_round_won;
	public float vol_game_lost = 1;
	public float vol_round_won = 1;

	public Color round_won_color = new Color(65, 146, 65, 1);
	public Color round_lost_color = new Color(146, 65, 65, 1);

	public TrapPlacement trap_manager = null;
	public List<GameObject> map_transfer_ui_elements;
	public List<GameObject> round_switch_ui_elements;

	public int level = 1;
	public bool autoload_next_level = true;
	public int num_robbers;
	public float robber_scare_time = 0.0f;

	private bool time_ticking;
	private float time_left;
	private float time_total = 0;

	private PMapLayer map;
	private KBCamController cam_controller;

	public bool IsRunning () {
		return (state == state_type.Running);
	}

	/**
	 * Returns a reference to the GameController instance.
	 * There should only be one, anyway.
	 */
	public static GameController GetCameController () {
		GameObject game_ctl_obj = GameObject.FindWithTag ("GameController");
		GameController game_ctl = null;

		if (game_ctl_obj != null)
			game_ctl = game_ctl_obj.GetComponent <GameController> ();
		return game_ctl;
	}

	// Use this for initialization
	void Start () {
		map = GameObject.FindGameObjectWithTag("Map").GetComponent<PMapLayer>();

		Reset ();
		NextMap ();

		audio_src = GetComponent <AudioSource> ();
	}

	void Reset () {
		state = state_type.Running;

		if (countdownText != null)
			countdownText.enabled = false;

		StartCountdown ();
	}

	void StartCountdown () {
		time_ticking = true;
		time_total = 0;
	}

	void UpdateCountdown () {
		int time = (int) time_left;

		if (countdownText != null) {
			countdownText.enabled = true;
			countdownText.text = "Defend for: " + time + " s";
		}
	}

	public void PlaySound(AudioClip clip, float volume) {
		if (audio_src == null)
			return;

		audio_src.PlayOneShot (clip, volume);
	}

	public void PlaySoundDelayed(AudioClip clip, float volume, float delay=0) {
		if (audio_src == null)
			return;

		audio_src.loop = false;
		audio_src.clip = clip;
		audio_src.volume = volume;
		audio_src.PlayDelayed (delay);
	}

	// Update is called once per frame
	void Update () {
		if (time_ticking) {
			time_left -= Time.deltaTime;
			time_total += Time.deltaTime;

			// Survived for the given amount of time?
			if (time_left < 0 || (time_total > 10.0f && map.GetNumRobbersLeft () == 0)) {
				RoundWon ();
			} else {
				UpdateCountdown ();
			}
		}
	}

	public void SpawnRobber () {
		map.SpawnRobber ();
	}

	public void StartMap () {
		float time = 0;

		StartCountdown ();

		for (int i = 0; i < num_robbers; i++) {
			Invoke ("SpawnRobber", time);
			time += RNGParkMiller.RandInt (2, 10);
		}
	}

	public void NextMap () {
		// Reset the map (if any).
		map.Reset ();

		// Scale number of rooms.
		//  Level 1 - 2: 3 rooms
		//  Level 3 - 4: 4 rooms
		//  Level 5 - 10: 5 rooms
		//  Level 10 - 21: 6 rooms
		//  ...
		map.desired_num_rooms = (uint) (3 + 5 * Mathf.Log (level, 10));

		// Scale map dimensions.
		//  Level 1: 20 tiles
		//  Level 2: 23 tiles
		//  Level 3: 25 tiles
		//  Level 4: 26 tiles
		//  Level 5: 27 tiles
		//  Level 6 - 7: 28 tiles
		//  Level 8 - 9: 29 tiles
		//  Level 10 - 12: 30 tiles
		//  ...
		map.width = (uint) (20 + 10 * Mathf.Log (level, 10));
		map.height = map.width;

		// Build the map.
		map.Build ();

		time_ticking = false;
		// 10 s per level + 1 s per tile of map width.
		time_left = 10 * level + map.width;

		// On higher levels robbers get over their fear quicker.
		if (robber_scare_time <= 0.0f)
			robber_scare_time = Mathf.Round(Mathf.Clamp(30 - 15 * Mathf.Log (level), 5, 30));
		print("Robber scare time: " + robber_scare_time + " " + Mathf.Clamp(30 - 15 * Mathf.Log (level), 5, 30));

		// Scale robbers.
		//  Level 1 - 3: 1 robber
		//  Level 4 - 5: 2 robbers
		//  Level 6 - 14: 3 robbers
		//  Level 15 - ...: 5 robbers
		num_robbers = (int) (1 + 2 * Mathf.Pow (2, 2 * (level - 10) / level));
		print("Num robbers: " + num_robbers);

		// Reset trap manager.
		if (trap_manager == null)
			trap_manager = GameObject.Find ("TrapManager").GetComponent<TrapPlacement> ();
		trap_manager.StartPlacingTraps ();

		// Reset camera controller.
		if (cam_controller == null)
			cam_controller = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<KBCamController> ();
		cam_controller.Reset ();
	}

	public List<int> GetTrapLimits () {
		List<int> limits = new List<int> (2);

		if (level >= 3) {
			limits.Add (level - 2); // Number of floor traps.
			limits.Add (level - 2); // Number of mummy traps.
		} else {
			limits.Add (0); // Number of floor traps.
			limits.Add (1); // Number of mummy traps.
		}

		return limits;
	}

	public void RoundWon () {
		if (state != state_type.RoundWon) {
			state = state_type.RoundWon;
			time_ticking = false;

			level++;

			if (countdownText != null)
				countdownText.enabled = false;

			PlaySound (sound_round_won, vol_round_won);

			if (autoload_next_level) {
				// Schedule the generation of the next map.
				StartCoroutine (ScheduleNextMap ());
				// Indicate that the player has won the round.
				StartCoroutine (ShowRoundSwitchPanel (true));
			} else {
				GameObject.Find ("UI").GetComponent<Pause> ().DoPause ("You won the round!");
			}
        }
    }

	IEnumerator ShowRoundSwitchPanel (bool won) {
		// Enable any round switch UI elements.
		foreach (GameObject obj in round_switch_ui_elements)
			obj.SetActive (true);
		
		// Show round number.
		Text roundtxt = GameObject.Find ("RoundText").GetComponent<Text> ();
		if (won) {
			// Note that by now we have already increased level number
			// and initialized the random-generator for the next map.
			roundtxt.text = "You've won round " + (level - 1) + "!";
			roundtxt.color = round_won_color;
		} else {
			roundtxt.text = "You've lost round " + level + "!";
			roundtxt.color = round_lost_color;
		}

		// Wait for a couple of seconds.
		yield return new WaitForSeconds (2);

		// Hide round number.
		foreach (GameObject obj in round_switch_ui_elements)
			obj.SetActive (false);
	}

	public void GameOver () {
		if (countdownText != null)
			countdownText.enabled = false;
		
		state = state_type.GameOver;
		time_ticking = false;

		PlaySoundDelayed (sound_game_lost, vol_game_lost, 1);

		if (autoload_next_level) {
			// Schedule the generation of the next map.
			StartCoroutine (ScheduleNextMap ());
			// Indicate that the player has lost the round.
			StartCoroutine (ShowRoundSwitchPanel (false));
		} else {
			GameObject.Find ("UI").GetComponent<Pause> ().DoPause ("Game Over!");
		}
	}

	IEnumerator ScheduleNextMap () {
		yield return new WaitForSeconds (2);

		foreach (GameObject obj in map_transfer_ui_elements) {
			if (obj != null)
				obj.SetActive (true);
		}

		Reset ();
		NextMap ();
	}
}
