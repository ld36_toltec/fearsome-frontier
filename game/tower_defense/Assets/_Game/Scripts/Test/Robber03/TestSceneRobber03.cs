using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TestSceneRobber03 : MonoBehaviour
{
	private MapLayerRobber03 map;
	
    // Start is called before the first frame update
    void Start()
    {
        map = GameObject.FindGameObjectWithTag("Map").GetComponent<MapLayerRobber03>();
		map.Build();
		
		SpawnRobber ();
    }

	public void SpawnRobber () {
		map.SpawnRobber ();
	}

    // Update is called once per frame
    void Update()
    {
		Robber robber;
		Vector2 pos;
		int room_idx = -1;

		if (map != null) {
			foreach (GameObject rob in map.GetRobberObjects ()) {
				robber = rob.GetComponent<Robber> ();
				if (robber != null && !robber.IsFalling ()) {
					pos = robber.GetMapPos ();
					room_idx = map.IsInRoom (pos);

					if (room_idx == 3)
						robber.FallHome ();
				}
			}
		}
    }
}
