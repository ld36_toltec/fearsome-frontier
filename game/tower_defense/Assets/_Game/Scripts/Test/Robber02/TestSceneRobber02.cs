using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TestSceneRobber02 : MonoBehaviour
{
	private MapLayerRobber02 map;
	
    // Start is called before the first frame update
    void Start()
    {
        map = GameObject.FindGameObjectWithTag("Map").GetComponent<MapLayerRobber02>();
		map.Build();
		
		SpawnRobber ();
		Invoke ("SpawnRobber", 2);
    }

	public void SpawnRobber () {
		map.SpawnRobber ();
	}

    // Update is called once per frame
    void Update()
    {
		Robber robber;
		Vector2 pos;

		if (map != null) {
			foreach (GameObject rob in map.GetRobberObjects ()) {
				robber = rob.GetComponent<Robber> ();
				if (robber != null) {
					pos = robber.GetMapPos ();
					if (pos.x > 3.5f && pos.y > 3.5f)
						robber.Scare ();
				}
			}
		}
    }
}
