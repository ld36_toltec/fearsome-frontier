using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TestSceneLamp01 : MonoBehaviour
{
	private MapLayerLamp01 map;
	
    // Start is called before the first frame update
    void Start()
    {
        map = GameObject.FindGameObjectWithTag("Map").GetComponent<MapLayerLamp01>();
		map.Build();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
