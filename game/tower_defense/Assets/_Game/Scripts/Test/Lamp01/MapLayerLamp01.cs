using UnityEngine;
using System.Collections.Generic;

// Delaunay triangulation is under MIT license.
// https://github.com/adamgit/Unity-delaunay
using Delaunay;
using Delaunay.Geo;

// Pathfinding
// https://github.com/RonenNess/Unity-2d-pathfinding
using PathFind;

public class MapLayerLamp01 : PMapLayerBase {
	/**
	 * Generate a small map with two rooms.
	 */
	void Generate () {
		uint i, j;
		traps = new List<GameObject> ();
		robbers = new List<GameObject> ();
		room_coords = new List<Vector2> ();
		room_rects = new List<Rect> ();
		room_graph_edges = new List<LineSegment> ();
		room_spanning_tree = new List<LineSegment> ();
		room_triangulation = new List<LineSegment> ();

		width = 12;
		height = 12;
		room_rects.Add(new Rect (1, 1, 2, 2));
		room_rects.Add(new Rect (4, 4, 2, 2));
		room_rects.Add(new Rect (8, 4, 2, 2));
		room_rects.Add(new Rect (8, 8, 2, 2));

		GenerateBlank ();
		foreach (Rect r in room_rects) {
			room_coords.Add(r.center);
			RectToBlockmap (r, (byte) PMapTile.tile_type.Floor);
		}
		Print();

		// Connect rooms.
		Delaunay.Voronoi v = new Delaunay.Voronoi (room_coords, null, new Rect (0, 0, width, height));
		room_graph_edges = v.VoronoiDiagram ();
		room_spanning_tree = v.SpanningTree (KruskalType.MINIMUM);
		room_triangulation = v.DelaunayTriangulation ();

		if (room_spanning_tree != null) {
			for (i = 0; i < room_spanning_tree.Count; i++) {
				LineSegment seg = room_spanning_tree [(int) i];
				LToBlockmap (seg, (byte) PMapTile.tile_type.Floor);
			}
			// A few additional corridors between rooms.
			for (i = 0; i < room_triangulation.Count; i++) {
				if (RNGParkMiller.RandInt (0, 10) > 5) {
					LineSegment seg = room_triangulation [(int) i];
					LToBlockmap (seg, (byte) PMapTile.tile_type.Floor);
				}
			}
		}

		// Find startingpoint and end.
		longest_route = FindLongestRoute();
		
		// Place a lamp into each room.
		for (i = 0; i < room_rects.Count; i++)
			PlaceLamp ((int) i, 1);

		// Generate tiles.
		tiles = new PMapTile[width, height];

		Vector3 pos = new Vector3 (0, 0, 0);
		for (i = 0; i < height; i++) {
			for (j = 0; j < width; j++) {
				pos.x = j;
				pos.z = i;
				tiles [j, i] = this.gameObject.AddComponent <PMapTile> ();
				tiles [j, i].SetSurroundings (GetSurroundings (j, i));
				tiles [j, i].Build (pos, blocks [j, i], tile_prefabs);
			}
		}
	}

	public void Build () {
		Generate ();
		Print ();

		BuildWalkableTilesmap();

		pathfind_grid = new PathFind.Grid((int) width, (int) height, tilesmap);
		pathfind_grid.Print();
	}
}
