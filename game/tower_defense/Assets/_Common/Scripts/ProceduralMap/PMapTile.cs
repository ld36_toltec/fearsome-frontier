﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PMapTile : MonoBehaviour {
	// Support for element names in tile prefab array, in Unity editor.
	[System.Serializable]
	public class TilePrefab {
		public string name;
		public GameObject prefab;
	}

	public enum tile_type: int {
		Void = 0,
		Wall = 1,
		Floor = 2,
		MummyTrap = 3,
		FloorTrap = 4,
		Lamp = 5,
		Chest = 6,
		Entrance = 7,
		Robber = 8
	};

	public Vector3 map_pos;
	public float size = 1;
	public tile_type type;
	protected byte[,] surroundings;

	private List<GameObject> childObjects = new List<GameObject> ();

	public void SetSurroundings (byte[,] blocks) {
		surroundings = blocks;
	}

	public tile_type GetNeighbour (uint x, uint y) {
		if (x < 3 && y < 3)
			return (tile_type) surroundings [x, y];
		return tile_type.Void;
	}

	public bool IsWalkable () {
		if (type != tile_type.Void &&
			type != tile_type.Wall &&
			type != tile_type.Lamp)
			return true;
		return false;
	}

	public static bool IsWalkable (tile_type ttype) {
		if (ttype != tile_type.Void &&
			ttype != tile_type.Wall &&
			ttype != tile_type.Lamp)
			return true;
		return false;
	}

	public Quaternion GetDirAwayFromWalls (int offset) {
		// Walkable W and E?
		if (IsWalkable (GetNeighbour (0, 1)) && IsWalkable (GetNeighbour (2, 1))) {
			return Quaternion.Euler (0, offset, 0);
		// Walkable N and S?
		} else if (IsWalkable (GetNeighbour (1, 0)) && IsWalkable (GetNeighbour (1, 2))) {
			return Quaternion.Euler (0, offset + 90, 0);
		// Walkable N and W?
		} else if (IsWalkable (GetNeighbour (1, 0)) && IsWalkable (GetNeighbour (0, 1))) {
			return Quaternion.Euler (0, offset + 135, 0);
		// Walkable W and S?
		} else if (IsWalkable (GetNeighbour (0, 1)) && IsWalkable (GetNeighbour (1, 2))) {
			return Quaternion.Euler (0, offset + 225, 0);
		// Walkable S and E?
		} else if (IsWalkable (GetNeighbour (1, 2)) && IsWalkable (GetNeighbour (2, 1))) {
			return Quaternion.Euler (0, offset - 45, 0);
		// Walkable E and N?
		} else if (IsWalkable (GetNeighbour (2, 1)) && IsWalkable (GetNeighbour (1, 0))) {
			return Quaternion.Euler (0, offset + 45, 0);
		// Some other configuration?
		} else {
			return Quaternion.Euler (0, RNGParkMiller.RandInt (0, 5) * 45, 0);
		}
	}

	//! \todo Make prefabs static, and initialized on map init.
	public GameObject Build (Vector3 pos, byte tile, TilePrefab[] prefabs) {
		GameObject prefab, prefab_floor, obj = null;

		type = (tile_type) tile;
		map_pos = pos;

		prefab = prefabs [(int) type].prefab;
		prefab_floor = prefabs [(int) tile_type.Floor].prefab;

		switch (type) {
		case tile_type.FloorTrap:
		case tile_type.MummyTrap:
			// Floor
			obj = (GameObject) Instantiate (prefab_floor, pos * size, Quaternion.Euler (-90, 0, 0));
			obj.transform.SetParent (gameObject.transform);
			obj.tag = "Trap";
			childObjects.Add (obj);

			// Trap
			pos += new Vector3 (0, 0.5f, 0);
			obj = (GameObject) Instantiate (prefab, pos * size, Quaternion.Euler (0, 0, 0));
			obj.transform.SetParent (gameObject.transform);
			obj.tag = "Trap";
			childObjects.Add (obj);
			break;
		case tile_type.Lamp:
			// Floor
			obj = (GameObject) Instantiate (prefab_floor, pos * size, Quaternion.Euler (-90, 0, 0));
			obj.transform.SetParent (gameObject.transform);
			obj.tag = "Lamp";
			childObjects.Add (obj);

			// Lamp, facing away from the walls.
			obj = (GameObject) Instantiate (prefab, pos * size, GetDirAwayFromWalls (0));
			obj.transform.SetParent (gameObject.transform);
			obj.tag = "Lamp";
			childObjects.Add (obj);
			break;
		case tile_type.Chest:
			// Floor
			obj = (GameObject) Instantiate (prefab_floor, pos * size, Quaternion.Euler (-90, 0, 0));
			obj.transform.SetParent (gameObject.transform);
			obj.tag = "Treasure";
			childObjects.Add (obj);

			// Chest
			obj = (GameObject) Instantiate (prefab, pos * size, GetDirAwayFromWalls (90));
			obj.transform.SetParent (gameObject.transform);
			obj.tag = "Treasure";
			childObjects.Add (obj);
			break;
		case tile_type.Entrance:
			// Floor
			obj = (GameObject)Instantiate (prefab_floor, pos * size, Quaternion.Euler (-90, 0, 0));
			obj.transform.SetParent (gameObject.transform);
			childObjects.Add (obj);
			break;
		case tile_type.Wall:
			// Wall
			pos += new Vector3 (0, 0.5f, 0);
			obj = (GameObject) Instantiate (prefab, pos * size, Quaternion.Euler (0, 0, 0));
			obj.transform.SetParent (gameObject.transform);
			childObjects.Add (obj);
			break;
		default:
			if (prefab != null) {
				obj = (GameObject) Instantiate (prefab, pos * size, Quaternion.Euler (-90, 0, 0));
				obj.transform.SetParent (gameObject.transform);
				childObjects.Add (obj);
			}
			break;
		}

		return obj;
	}

	public void Clear () {
		foreach (var obj in childObjects)
			Destroy (obj);
		type = tile_type.Void;
	}

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {

	}

	void OnMouseDown () {
		
	}
}
