using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;


public class PMapLayerStatic : PMapLayerBase
{
	// Map layout as a string with indexes to tile prefabs.
	[TextArea(3, 10)]
	public string map_layout;

	public void Start () {
		Build();
	}

	public void Build () {
		byte[,] eroded_blocks;

		GenerateFromLayout (map_layout);
		Print ();
		eroded_blocks = GetErosionMap(width, height, blocks);
		Print (width, height, eroded_blocks);
	}

	protected void GenerateFromLayout (string layout) {
		List<string> lines = new List<string>();
		string line;
		uint num_cols = 0;

		using (StringReader sr = new StringReader(layout)) {
			while ((line = sr.ReadLine()) != null) {
				if (line.Length > num_cols)
					num_cols = (uint) line.Length;
				lines.Add(line);
			}
		}
		width = num_cols;
		height = (uint) lines.Count;
		blocks = new byte[width, height];

		for (int y = 0; y < height; y++) {
			line = lines [y];
			for (int x = 0; x < line.Length; x++) {
				blocks [x, y] = (byte) (line[x] - '0');
			}
		}
	}

	protected bool IsRoomTileForErosion (byte block) {
		if ((block == (byte) PMapTile.tile_type.Void) &&
			(block == (byte) PMapTile.tile_type.Wall))
			return false;
		return true;
	}

	/**
	 * Erode the blocks array tile by tile, returning the number of iterations that each tile lasted.
	 * With iteration 1, roads and room edges are removed. The larger the iteration, the larger the room.
	 */
	protected byte[,] GetErosionMap (uint blocks_width, uint blocks_height, byte[,] blocks_in) {
		byte[,] blocks_out = new byte[blocks_width, blocks_height];

		uint iteration = 1;
		bool eroded_something;
		do {
			eroded_something = false;

			for (uint y = 0; y < blocks_height; y++) {
				for (uint x = 0; x < blocks_width; x++) {
					if (blocks_out [x, y] == 0) {
						eroded_something = true;
						if (x == 0 || y == 0 || x == blocks_width - 1 || y == blocks_height - 1) {
							blocks_out [x, y] = (byte) iteration;
						} else if (IsRoomTileForErosion(blocks_in [x, y]) &&
							(blocks_out [x - 1, y] > 0 && blocks_out [x - 1, y] < iteration) ||
							(blocks_out [x + 1, y] > 0 && blocks_out [x + 1, y] < iteration) ||
							(blocks_out [x, y - 1] > 0 && blocks_out [x, y - 1] < iteration) ||
							(blocks_out [x, y + 1] > 0 && blocks_out [x, y + 1] < iteration)) {
							blocks_out [x, y] = (byte) iteration;
						}
					}
				}
			}
			iteration += 1;
			if (iteration > math.max(blocks_width, blocks_height)) {
				Debug.LogError ("Failed to calculate erosion map.");
				break;
			}
		} while (eroded_something);

		return blocks_out;
	}

	// TODO:: Find room coordinates
	// TODO:: Find room rects
	// TODO:: Find room connectivity
}
