﻿using UnityEngine;
using System.Collections.Generic;

// Delaunay triangulation is under MIT license.
// https://github.com/adamgit/Unity-delaunay
using Delaunay;
using Delaunay.Geo;

// Pathfinding
// https://github.com/RonenNess/Unity-2d-pathfinding
using PathFind;

public class PMapLayer : PMapLayerBase {
	// Number of rooms used in the dungeon generator.
	public uint desired_num_rooms;
	// The probability of having a lamp in the room.
	public float lamp_probability = 0.6f;

	/**
	 * Uses a dungeon generator to produce the map.
	 */
	void GenerateRandom () {
		Rect[] rooms = new Rect [desired_num_rooms];
		int room_index = 0;
		bool retry = false;
		Rect mapRect = new Rect (-1, -1, width + 2, height + 2);
		Rect r = new Rect();
		uint i, j, timeout;
		int max_room_size = 3;
		Vector2 room;

		GenerateBlank ();

		traps = new List<GameObject> ();
		robbers = new List<GameObject> ();
		room_coords = new List<Vector2> ();
		room_rects = new List<Rect> ();
		room_graph_edges = new List<LineSegment> ();
		room_spanning_tree = new List<LineSegment> ();
		room_triangulation = new List<LineSegment> ();

		// Room size depending on map size.
		if (width > 10 && height > 10)
			max_room_size = (int) (Mathf.Sqrt (width * height) * 0.15f);

		// Generate pseudo-random rooms.
		for (i = 0; i < desired_num_rooms; i++) {
			for (timeout = 0; timeout < 10 * desired_num_rooms; timeout++) {
				// Random rect for the room.
				//  Minimum room size 2 x 2 tiles, for 1 x 1 would be the size of a tunnel.
				//  Account for an outline of wall tiles.
				r.x = RNGParkMiller.RandInt (1, (int) width - 2);
				r.y = RNGParkMiller.RandInt (1, (int) height - 2);
				r.width = RNGParkMiller.RandInt (2, max_room_size + 1);
				r.height = RNGParkMiller.RandInt (2, max_room_size + 1);

				retry = false;

				// Check if it extends beyond the map.
				if (!mapRect.Contains (r.min) || !mapRect.Contains (r.max)) {
					//print ("Overlaps map edge " + r);
					retry = true;
				} else {
					// Check if it overlaps another room.
					for (j = 0; j < room_index; j++) {
						if (rooms [j].Overlaps (r)) {
							//print ("Overlaps map another room " + r);
							retry = true;
							break;
						}
					}
				}

				// Retry until there's no more overlap or until there's a timeout.
				if (!retry) {
					rooms [room_index] = r;
					room_rects.Add (r);
					room_coords.Add (r.center);

					RectToBlockmap (r, (byte) PMapTile.tile_type.Floor);

					// Place a lamp.
					PlaceLamp ((int) i, lamp_probability);

					room_index++;
					break;
				}
			}
			// Abort - cannot fit the desired number of rooms.
			if (retry) {
				Debug.Log ("Only managed to fit " + room_index + " rooms to the map, whereas " + desired_num_rooms + " were desired.");
				break;
			}
		}

		// Connect rooms.
		Delaunay.Voronoi v = new Delaunay.Voronoi (room_coords, null, new Rect (0, 0, width, height));
		room_graph_edges = v.VoronoiDiagram ();
		room_spanning_tree = v.SpanningTree (KruskalType.MINIMUM);
		room_triangulation = v.DelaunayTriangulation ();

		if (room_spanning_tree != null) {
			for (i = 0; i < room_spanning_tree.Count; i++) {
				LineSegment seg = room_spanning_tree [(int) i];
				LToBlockmap (seg, (byte) PMapTile.tile_type.Floor);
			}
			// A few additional corridors between rooms.
			for (i = 0; i < room_triangulation.Count; i++) {
				if (RNGParkMiller.RandInt (0, 10) > 5) {
					LineSegment seg = room_triangulation [(int) i];
					LToBlockmap (seg, (byte) PMapTile.tile_type.Floor);
				}
			}
		}

		// Find startingpoint and end.
		longest_route = FindLongestRoute();

		// Place an entrance.
		room = room_coords [longest_route [0]];
		//! \todo Instead of room center, pick a wall for the entrance.

		Debug.Log ("Entrance " + room.x + ", " + room.y);
		SetTile ((uint) room.x, (uint) room.y, (byte) PMapTile.tile_type.Entrance);

		// Place a chest.
		room = room_coords [longest_route [1]];
		Debug.Log ("Chest " + room.x + ", " + room.y);
		SetTile ((uint) room.x, (uint) room.y, (byte) PMapTile.tile_type.Chest);

		// Generate tiles.
		tiles = new PMapTile[width, height];

		Vector3 pos = new Vector3 (0, 0, 0);
		for (i = 0; i < height; i++) {
			for (j = 0; j < width; j++) {
				pos.x = j;
				pos.z = i;
				tiles [j, i] = this.gameObject.AddComponent <PMapTile> ();
				tiles [j, i].SetSurroundings (GetSurroundings (j, i));
				tiles [j, i].Build (pos, blocks [j, i], tile_prefabs);
			}
		}
	}

	public void Build () {
		GenerateRandom ();
		Print ();

		BuildWalkableTilesmap();

		pathfind_grid = new PathFind.Grid((int) width, (int) height, tilesmap);
		pathfind_grid.Print();
	}
}
