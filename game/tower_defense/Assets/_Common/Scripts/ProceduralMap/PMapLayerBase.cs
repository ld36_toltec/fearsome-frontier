﻿using UnityEngine;
using System.Collections.Generic;

// Delaunay triangulation is under MIT license.
// https://github.com/adamgit/Unity-delaunay
using Delaunay;
using Delaunay.Geo;

// Pathfinding
// https://github.com/RonenNess/Unity-2d-pathfinding
using PathFind;

public class PMapLayerBase : MonoBehaviour {
	// Single map layer in characters.
	protected byte[,] blocks;
	protected PMapTile[,] tiles;

	// Delaunay triangulation for connecting rooms.
	protected List<Vector2> room_coords;
	protected List<Rect> room_rects;
	protected List<LineSegment> room_graph_edges;
	protected List<LineSegment> room_spanning_tree;
	protected List<LineSegment> room_triangulation;

	// Support for multiple types of walls, floors.
	public PMapTile.TilePrefab[] tile_prefabs = new PMapTile.TilePrefab[9];
	// null, wall, floor, trap_mummy, trap_floor, lamp, chest, robber

	protected List<GameObject> robbers;
	protected List<GameObject> traps;
	protected List<GameObject> lamps;

	// Map dimensions in tiles.
	public uint width;
	public uint height;

	protected int[] longest_route;

	protected float[,] tilesmap;
	protected PathFind.Grid pathfind_grid;


	public List<GameObject> GetRobberObjects () {
		return robbers;
	}

	public int GetNumRobbersLeft () {
		Robber robber;
		int num = 0;

		foreach (GameObject rob in robbers) {
			robber = rob.GetComponent<Robber> ();
			if (robber.IsStillExploring ())
				num++;
		}

		return num;
	}

	/**
	 * Set tile type.
	 */
	public void SetTile (uint x, uint y, byte val) {
		if (x < width && y < height)
			blocks [x, y] = val;
	}

	/**
	 * Get tile type. 
	 */
	public byte GetTile (uint x, uint y) {
		if (x < width && y < height)
			return blocks [x, y];
		return (byte) PMapTile.tile_type.Void;
	}

	/**
	 * Get the immediate surroundings around the tile.
	 */
	public byte[,] GetSurroundings (uint x, uint y) {
		byte[,] surroundings = new byte[3, 3];
		for (int i = -1; i < 2; i++) {
			for (int j = -1; j < 2; j++) {
				if (y + i >= 0 && x + j >= 0 && y + i < height && x + j < width)
					surroundings [1 + j, 1 + i] = blocks [x + j, y + i];
			}
		}
		return surroundings;
	}

	public List<Vector2> GetRoomDoorways (int room_index) {
		List<Vector2> doorways = new List<Vector2> ();

		if (room_index >= 0 && room_index < room_rects.Count) {
			List<Vector2> coords = new List<Vector2> ();
			Rect r = room_rects[room_index];

			// Check the tiles on room edges.
			for (int i = 0; i < r.height; i++) {
				coords.Add (new Vector2 (r.x - 1, r.y + i));
				coords.Add (new Vector2 (r.x + r.width + 1, r.y + i));
			}
			for (int j = 0; j < r.width; j++) {
				coords.Add (new Vector2 (r.x + j, r.y - 1));
				coords.Add (new Vector2 (r.x + j, r.y + r.height + 1));
			}
			foreach (Vector2 c in coords) {
				if (PMapTile.IsWalkable ((PMapTile.tile_type) GetTile ((uint) c.x, (uint) c.y))) {
					// Shift the coordinates back into the room rect.
					// W
					if (c.x < r.x)
						doorways.Add (new Vector2 (c.x + 1, c.y));
					// E
					else if (c.x > r.x + r.width)
						doorways.Add (new Vector2 (c.x - 1, c.y));
					// N
					else if (c.y < r.y)
						doorways.Add (new Vector2 (c.x, c.y + 1));
					// S
					else if (c.y > r.y + r.height)
						doorways.Add (new Vector2 (c.x, c.y - 1));
				}
			}
		}
		return doorways;
	}

	/**
	 * Sets a floor tile, surrounded by walls.
	 */
	public void SetRoomTile (uint x, uint y, byte val) {
		Vector2[] kernel = new [] {
			new Vector2 (-1, -1), new Vector2 (0, -1), new Vector2 (1, -1), new Vector2 (1, 0), 
			new Vector2 (1, 1), new Vector2 (0, 1), new Vector2 (-1, 1), new Vector2 (-1, 0)
		};

		if (x == 0 || x == width - 1 || y == 0 || y == height - 1) {
			//Debug.Log ("Ignoring room tile on map edge.");
			return;
		}

		SetTile (x, y, val);

		foreach (Vector2 v in kernel) {
			if (GetTile (x + (uint) v.x, y + (uint) v.y) == (byte)PMapTile.tile_type.Void) {
				SetTile (x + (uint) v.x, y + (uint) v.y, (byte)PMapTile.tile_type.Wall);
			}
		}
	}

	public void Reset () {
		int i, j;

		if (tiles != null) {
			for (i = 0; i < height; i++) {
				for (j = 0; j < width; j++) {
					tiles [j, i].Clear ();
					Destroy (tiles [j, i]);
				}
			}
		}

		if (robbers != null) {
			foreach (GameObject robber in robbers)
				Destroy (robber);
		}

		if (traps != null) {
			foreach (GameObject trap in traps)
				Destroy (trap);
		}
	}

	/**
	 * Generates a blank map with the provided dimensions. 
	 */
	protected void GenerateBlank () {
		uint i, j;

		blocks = new byte[width, height];

		for (i = 0; i < height; i++) {
			for (j = 0; j < width; j++) {
				blocks [j, i] = (byte) PMapTile.tile_type.Void;
			}
		}
	}

	/**
	 * Draws a rect of room tiles on blockmap.
	 */
	protected void RectToBlockmap (Rect r, byte val) {
		for (uint y = (uint) r.y; y <= (uint) r.yMax; y++) {
			for (uint x = (uint) r.x; x <= (uint) r.xMax; x++) {
				SetRoomTile (x, y, val);
			}
		}
	}

	/**
	 * Draws an L-shaped line of room tiles on blockmap.
	 */
	protected void LToBlockmap (LineSegment segment, byte val) {
		Vector2 p0 = segment.p0.Value;
		Vector2 p1 = segment.p1.Value;
		Vector2 d = p1 - p0;
		Vector2 min, max;
		int i, nx, ny;

		nx = (int) Mathf.Sign(d.x);
		ny = (int) Mathf.Sign(d.y);

		//! \todo Turn into a dedicated rect expansion function later.
		if (p0.x < p1.x) {
			min.x = p0.x;
			max.x = p1.x;
		} else {
			min.x = p1.x;
			max.x = p0.x;
		}
		if (p0.y < p1.y) {
			min.y = p0.y;
			max.y = p1.y;
		} else {
			min.y = p1.y;
			max.y = p0.y;
		}

		// Draw the L.
		if (Mathf.Abs(d.x) > Mathf.Abs(d.y)) {
			if (nx != 0) {
				for (i = (int)p0.x; i != (int)p1.x; i += nx)
					SetRoomTile ((uint)i, (uint)p0.y, val);
			}

			if (ny != 0) {
				for (i = (int)p0.y; i != (int)p1.y; i += ny)
					SetRoomTile ((uint)p1.x, (uint)i, val);
			}
		} else {
			if (ny != 0) {
				for (i = (int)p0.y; i != (int)p1.y; i += ny)
					SetRoomTile ((uint)p0.x, (uint)i, val);
			}
			if (nx != 0) {
				for (i = (int)p0.x; i != (int)p1.x; i += nx)
					SetRoomTile ((uint)i, (uint)p1.y, val);
			}
		}
	}

	protected void PlaceLamp (int room_index, float probability) {
		List<Vector2> dirs = new List<Vector2> () {
			new Vector2 (-1, -1), new Vector2 (0, -1), new Vector2 (1, -1), new Vector2 (1, 0), 
			new Vector2 (1, 1), new Vector2 (0, 1), new Vector2 (-1, 1), new Vector2 (-1, 0)
		};

		//! \note Should not place anything at the center of the room.

		Rect r = room_rects [room_index];

		// Should not place anything at doorways.
		// Remove all directions which are at doorways.
		List<Vector2> dirs_to_remove = new List<Vector2> ();
		List<Vector2> doorways = GetRoomDoorways (room_index);
		for (int i = 0; i < dirs.Count; i++) {
			foreach (Vector2 doorway in doorways) {
				if ((r.center + dirs[i] - doorway).magnitude < 1.0f)
					dirs_to_remove.Add (dirs[i]);
			}
		}
		foreach (Vector2 dir in dirs_to_remove)
			dirs.Remove(dir);

		// Pick a random direction from the room center.
		if (RNGParkMiller.RandInt (0, 100) > 100 * (1 - probability)) {
			Vector2 v = new Vector2 ();
			v = dirs [RNGParkMiller.RandInt (0, dirs.Count)];
			v += r.center;

			// Place a lamp there if the tile is free.
			if (GetTile ((uint) v.x, (uint) v.y) == (byte) PMapTile.tile_type.Floor)
				SetTile ((uint) v.x, (uint) v.y, (byte) PMapTile.tile_type.Lamp);
		}
	}

	public GameObject SpawnRobber () {
		if (room_coords == null)
			return null;

		Vector2 room = room_coords [longest_route [0]];
		GameObject prefab = tile_prefabs [(int) PMapTile.tile_type.Robber].prefab;
		GameObject robber = (GameObject) Instantiate (prefab, new Vector3 (room.x, 0, room.y), Quaternion.Euler (0, 0, 0));
		robber.tag = "Robber";
		robbers.Add (robber);
		return robber;
	}

	public bool IsFreeTile (Vector2 pos) {
		return (GetTile ((uint)pos.x, (uint)pos.y) == (byte)PMapTile.tile_type.Floor);
	}

	public GameObject SpawnTrap (PMapTile.tile_type type, Vector2 pos) {
		GameObject trap = null;
		uint i, j;

		j = (uint) pos.x;
		i = (uint) pos.y;
		SetTile (j, i, (byte) type);
		// Destroy the old tile.
		tiles [j, i].Clear ();
		Destroy (tiles [j, i]);
		// Build a new one, with an embedded trap.
		tiles [j, i] = this.gameObject.AddComponent <PMapTile> ();
		tiles [j, i].SetSurroundings (GetSurroundings (j, i));
		trap = tiles [j, i].Build (new Vector3 (j, 0, i), blocks [j, i], tile_prefabs);
		trap.tag = "Trap";

		return trap;
	}

	/**
	 * TODO:: Make this more correct. 
	 */
	protected int[] FindLongestRoute () {
		float d, max_d = 0;
		int[] max_i = new int[2];
		int i, j;

		for (i = 0; i < room_coords.Count; i++) {
			for (j = 0; j < room_coords.Count; j++) {
				d = Vector3.Distance (room_coords [i], room_coords [j]);
				if (d > max_d) {
					max_d = d;
					max_i [0] = i;
					max_i [1] = j;
				}
			}
		}
		return max_i;
	}

	public int[] GetLongestRoute () {
		return longest_route;
	}

	public int GetRoomCount () {
		return room_coords.Count;
	}

	protected void BuildWalkableTilesmap () {
		uint i, j;
		PMapTile tile;

		tilesmap = new float[width, height];
		for (i = 0; i < height; i++) {
			for (j = 0; j < width; j++) {
				tile = tiles[j, i];
				if (tile.IsWalkable()) {
					tilesmap [j, i] = 1.0f;
				} else {
					tilesmap [j, i] = 0.0f;
				}
			}
		}
	}

	public List<PathFind.Point> FindPath (int room_index1, int room_index2) {
		List<PathFind.Point> path = null;
		PathFind.Point _from = new PathFind.Point ();
		PathFind.Point _to = new PathFind.Point ();

		if (pathfind_grid == null)
			return path;

		print("FindPath " + room_index1 + ", " + room_index2 + " out of " + room_coords.Count);
		_from = new PathFind.Point((int) room_coords[room_index1].x, (int) room_coords[room_index1].y);
		_to = new PathFind.Point((int) room_coords[room_index2].x, (int) room_coords[room_index2].y);
		path = PathFind.Pathfinding.FindPath (pathfind_grid, _from, _to);

		string txt = "Path from " + room_index1 + " to " + room_index2 + "\r\n";

		foreach (PathFind.Point p in path) {
			txt += "p " + p.x + ", " + p.y + " -> ";
		}

		//Debug.Log(txt);

		return path;
	}

	public List<PathFind.Point> FindPath (Vector2 pos, int room_index) {
		List<PathFind.Point> path = null;
		PathFind.Point _from = new PathFind.Point ();
		PathFind.Point _to = new PathFind.Point ();

		if (pathfind_grid == null)
			return path;

		_from = new PathFind.Point((int) pos.x, (int) pos.y);
		_to = new PathFind.Point((int) room_coords[room_index].x, (int) room_coords[room_index].y);
		path = PathFind.Pathfinding.FindPath (pathfind_grid, _from, _to);
		/*
		print ("Path " + path);

		foreach (PathFind.Point p in path) {
			print ("p " + p.x + ", " + p.y);
		}*/

		return path;
	}

	/**
	 * Checks if a point is in a room and returns the room index.
	 * Returns -1 if the point is not in a room.
	 */
	public int IsInRoom (Vector2 coords) {
		for (int i = 0; i < room_rects.Count; i++) {
			if (room_rects [i].Contains (coords))
				return i;
		}
		return -1;
	}

	public Vector2 GetRoomPos (int room) {
		return room_coords [room];
	}

	public int GetRoomIndex (Vector2 pos) {
		for (int i = 0; i < room_coords.Count; i++) {
			if (room_coords [i] == pos)
				return i;
		}

		return -1;
	}

	public List<int> GetNeighborRooms (int room) {
		Vector2 room_pos;
		List<int> neighbors = new List<int> ();
		LineSegment segment;

		if (room >= 0 && room < room_coords.Count)
			room_pos = room_coords [room];
		else
			return neighbors;

		for (int i = 0; i < room_spanning_tree.Count; i++) {
			segment = room_spanning_tree [i];
			// One of the segment endpoints is in this room.
			// Thus, the other endpoint must be a neighbor.
			if (segment.p0.Value == room_pos)
				neighbors.Add (GetRoomIndex (segment.p1.Value));
			if (segment.p1.Value == room_pos)
				neighbors.Add (GetRoomIndex (segment.p0.Value));
		}

		return neighbors;
	}

	/**
	 * Prints the map in ASCII.
	 */
	public void Print () {
		int i, j;
		string text = "";

		for (i = 0; i < height; i++) {
			for (j = 0; j < width; j++) {
				text += (char) (blocks [j, i] + '0');
			}
			text += "\r\n";
		}
		print (text);
	}

	public void Print (uint blocks_width, uint blocks_height, byte[,] blocks_in) {
		int i, j;
		string text = "";

		for (i = 0; i < blocks_height; i++) {
			for (j = 0; j < blocks_width; j++) {
				text += (char) (blocks_in [j, i] + '0');
			}
			text += "\r\n";
		}
		print (text);
	}

	// Based on https://gist.github.com/Arakade/9dd844c2f9c10e97e3d0
	static public void drawString(string text, Vector3 worldPos, Color? colour = null) {
		Vector3 screenPos;
		Camera cam;

		UnityEditor.Handles.BeginGUI();

		var restoreColor = GUI.color;

		if (colour.HasValue) GUI.color = colour.Value;

		var view = UnityEditor.SceneView.currentDrawingSceneView;
		if (view == null) {
			GameObject main_cam = GameObject.FindGameObjectWithTag("MainCamera");
			cam = main_cam.GetComponent<Camera>();
		} else {
			cam = view.camera;
		}
		screenPos = cam.WorldToScreenPoint(worldPos);

		if (screenPos.y < 0 || screenPos.y > Screen.height || screenPos.x < 0 || screenPos.x > Screen.width || screenPos.z < 0) {
			GUI.color = restoreColor;
			UnityEditor.Handles.EndGUI();
			return;
		}

		Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));
		GUI.Label(new Rect(screenPos.x - (size.x / 2), -screenPos.y + cam.pixelHeight + 4, size.x, size.y), text);
		GUI.color = restoreColor;
		UnityEditor.Handles.EndGUI();
	}

	/**
	 * Draws debug info for the dungeon generator.
	 */
	public void OnDrawGizmos () {
		float y0 = 1.5f;
		Vector3 v = new Vector3 ();
		Gizmos.color = Color.red;
		if (room_coords != null) {
			for (int i = 0; i < room_coords.Count; i++) {
				v = new Vector3 (room_coords [i].x, y0, room_coords [i].y);

				if (i == longest_route [0] || i == longest_route [1])
					Gizmos.color = Color.blue;

				Gizmos.DrawSphere (v, 0.2f);
				Gizmos.color = Color.red;
				drawString("r" + i, v, Color.white);
			}
		}

		if (room_graph_edges != null) {
			Gizmos.color = Color.white;
			for (int i = 0; i < room_graph_edges.Count; i++) {
				Vector2 left = (Vector2)room_graph_edges [i].p0;
				Vector2 right = (Vector2)room_graph_edges [i].p1;
				Gizmos.DrawLine (new Vector3 (left.x, y0, left.y), new Vector3 (right.x, y0, right.y));
			}
		}

		Gizmos.color = Color.magenta;
		if (room_triangulation != null) {
			for (int i = 0; i< room_triangulation.Count; i++) {
				Vector2 left = (Vector2)room_triangulation [i].p0;
				Vector2 right = (Vector2)room_triangulation [i].p1;
				Gizmos.DrawLine (new Vector3 (left.x, y0, left.y), new Vector3 (right.x, y0, right.y));
			}
		}

		if (room_spanning_tree != null) {
			Gizmos.color = Color.green;
			for (int i = 0; i< room_spanning_tree.Count; i++) {
				LineSegment seg = room_spanning_tree [i];
				Vector2 left = (Vector2)seg.p0;
				Vector2 right = (Vector2)seg.p1;
				Gizmos.DrawLine (new Vector3 (left.x, y0, left.y), new Vector3 (right.x, y0, right.y));
			}
		}

		Gizmos.color = Color.yellow;
		Gizmos.DrawLine (new Vector3 (0, y0, 0), new Vector3 (0, y0, height));
		Gizmos.DrawLine (new Vector3 (0, y0, 0), new Vector3 (width, y0, 0));
		Gizmos.DrawLine (new Vector3 (width, y0, 0), new Vector3 (width, y0, height));
		Gizmos.DrawLine (new Vector3 (0, y0, height), new Vector3 (width, y0, height));
	}
}
