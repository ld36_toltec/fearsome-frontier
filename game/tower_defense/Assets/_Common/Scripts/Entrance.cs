﻿using UnityEngine;
using System.Collections;

public class Entrance : MonoBehaviour {

	private Animator anim;
	public float speed = 3.0f;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();

		anim.speed = speed;
		anim.Play ("Default Take");

	}
	
	// Update is called once per frame
	void Update () {
	}
}
