﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TrapPlacement : MonoBehaviour {

    bool isPlacingTraps = true;
    public GameObject selectorPrefab;
    public Material selectionMaterial;
    GameObject selector;
	PMapTile.tile_type trapIndex;

	private GameController gameController;
	private List<int> trapLimits;
	private List<int> trapCount;
	public Text floorTrapButtonText;
	public Text mummyTrapButtonText;

	private PMapLayer map;

	// Use this for initialization
	void Start () {
		trapLimits = new List<int> ();
		trapLimits.Add (0);
		trapLimits.Add (0);

		ResetCount ();

		gameController = (GameController) GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
		map = GameObject.FindGameObjectWithTag("Map").GetComponent<PMapLayer>();
        selector = (GameObject) Instantiate (selectorPrefab, Vector3.zero, Quaternion.identity);
		selector.transform.SetParent (map.transform);
		selector.tag = "Selector";
		StartPlacingTraps ();
    }

	public GameObject SpawnTrap (PMapTile.tile_type type, Vector3 pos) {
		Vector2 v = new Vector2 (pos.x, pos.z);

		if (map.IsFreeTile (v)) {
			if (type == PMapTile.tile_type.FloorTrap) {
				if (trapCount [0] >= trapLimits [0])
					return null;
			}
			if (type == PMapTile.tile_type.MummyTrap) {
				if (trapCount [1] >= trapLimits [1])
					return null;
			}

			GameObject trap = map.SpawnTrap (type, v);

			if (trap != null) {
				if (type == PMapTile.tile_type.FloorTrap)
					trapCount [0]++;
				if (type == PMapTile.tile_type.MummyTrap)
					trapCount [1]++;
			}
			return trap;
		} else {
			return null;
		}
	}

    // Update is called once per frame
    void Update () {
		if (isPlacingTraps) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) {
                GameObject curObject = hit.collider.gameObject;

				// Update selector position if there's nothing in the way. Force it onto the floor.
				//! \note All components of the prefab must be tagged for this to work.
				if (selector != null && curObject.tag == "Untagged")
					selector.transform.position = curObject.transform.position + new Vector3(0.0f, 0.5f, 0.0f);
				// Mouse clicked, and mouse not over UI?
				if (Input.GetMouseButton (0) && !EventSystem.current.IsPointerOverGameObject ()) {
					SpawnTrap (trapIndex, curObject.transform.position);
				}
            }

			// Update trap counts and limits.
			if (floorTrapButtonText != null && trapCount.Count > 0 && trapLimits.Count > 0)
				floorTrapButtonText.text = "Pit\r\n" + trapCount[0] + "/" + trapLimits[0];
			if (mummyTrapButtonText != null && trapCount.Count > 1 && trapLimits.Count > 1)
				mummyTrapButtonText.text = "Coffin\r\n" + trapCount[1] + "/" + trapLimits[1];
        }
	}

	public void ResetCount() {
		trapCount = new List<int> ();
		trapCount.Add (0);
		trapCount.Add (0);
	}

	public void StartPlacingTraps() {
		if (gameController == null)
			return;

		if (selector != null)
			selector.SetActive (true);
		isPlacingTraps = true;

		ResetCount ();
		trapLimits = gameController.GetTrapLimits ();

		SelectMummyTrap ();
	}

    public void StopPlacingTraps() {
		if (selector != null)
			selector.SetActive (false);
        isPlacingTraps = false;
    }

    public void SelectFloorTrap() {
		trapIndex = PMapTile.tile_type.FloorTrap;
    }

	public void SelectMummyTrap() {
		trapIndex = PMapTile.tile_type.MummyTrap;
	}
}
