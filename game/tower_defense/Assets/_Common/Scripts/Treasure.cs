﻿using UnityEngine;
using System.Collections;

public class Treasure : MonoBehaviour {

	public float min_robber_distance;

	private AudioSource audio_src;
	public AudioClip creak_low;
	public float creak_low_vol = 1;
	public AudioClip creak_med;
	public float creak_med_vol = 1;

	// Use this for initialization
	void Start () {
		audio_src = GetComponent <AudioSource> ();
	}

	public void PlaySound(AudioClip clip, float volume, bool loop=false) {
		if (audio_src == null)
			return;

		if (loop) {
			audio_src.clip = clip;
			audio_src.volume = volume;
			audio_src.Play();
		} else {
			audio_src.PlayOneShot (clip, volume);
		}
	}

	public void StopSound() {
		if (audio_src == null)
			return;

		audio_src.Stop();
	}

	void SignalGameOver () {
		GameController game_ctl = GameController.GetCameController ();

		if (game_ctl != null) {
			if (game_ctl.IsRunning ()) {
				PlaySound (creak_med, creak_med_vol);
				game_ctl.GameOver ();
			}
		} else {
			Debug.Log ("Cannot find a GameController object for playing sounds.");
		}
	}

	void OnTriggerEnter (Collider other) {
		if (other.tag == "Robber") {
			SignalGameOver ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
