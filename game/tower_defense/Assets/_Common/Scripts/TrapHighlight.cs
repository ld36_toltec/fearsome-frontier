﻿using UnityEngine;
using System.Collections;

public class TrapHighlight : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown () {
		GameObject child;

		MeshRenderer mr = GetComponent<MeshRenderer> ();

		for (int i = 0; i < this.transform.childCount; i++) {
			child = this.transform.GetChild (i).gameObject;
			if (child.gameObject.CompareTag ("Trap")) {
				child.gameObject.BroadcastMessage ("OnMouseDown");
				// Hide the trap highlight once the trap is highlighted.
				if (mr != null)
					mr.enabled = false;
			}
		}
	}
}
