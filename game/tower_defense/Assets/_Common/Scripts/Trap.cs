﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Trap : MonoBehaviour {
	public enum state_type: byte
	{
		Disabled = 0,
		Closed,
		Opened
	};

	public enum action_type: byte
	{
		Idle = 0,
		Opening,
		Closing
	};

	public state_type state;
	public action_type action;
	protected AudioSource audio_src;

	public AudioClip thump_low;
	public float thump_low_vol = 1;
	public AudioClip thump_med;
	public float thump_med_vol = 1;
	public AudioClip stone_slide;
	public float stone_slide_vol = 1;

	public Vector3 open_motion = new Vector3 (0, 1, 0);

	protected GameObject[] robbers;
	protected List<int> robbers_in_proximity = new List<int> ();

	//! \note Doesn't work with public.
	protected float trigger_distance = 2.0f;

	protected int use_count = 0;
	protected int max_use_count = 1;

	public void Start () {
		audio_src = GetComponent <AudioSource> ();
	}

	public void PlaySound(AudioClip clip, float volume, bool loop=false) {
		if (audio_src == null)
			return;

		if (loop) {
			audio_src.clip = clip;
			audio_src.volume = volume;
			audio_src.Play();
		} else {
			audio_src.PlayOneShot (clip, volume);
		}
	}

	public void StopSound() {
		if (audio_src == null)
			return;

		audio_src.Stop();
	}

	public IEnumerator Slide (Trap target, Vector3 end_position, float speed) {
		PlaySound (stone_slide, stone_slide_vol, true);

		// Move the block gradually.
		while (target.transform.position != end_position) {
			target.transform.position = Vector3.MoveTowards (target.transform.position, end_position, speed * Time.deltaTime);
			yield return new WaitForEndOfFrame ();
		}

		StopSound ();

		if (target.action == Trap.action_type.Opening) {
			target.state = Trap.state_type.Opened;
			PlaySound (thump_med, thump_med_vol);
		} else if (target.action == Trap.action_type.Closing) {
			target.state = Trap.state_type.Closed;
			PlaySound (thump_low, thump_low_vol);
		}

		target.action = Trap.action_type.Idle;
	}
	
	// Update is called once per frame
	void Update () {
		//! \note Doesn't get called due to inheritance.
	}

	protected void OnMouseDown () {
		if (state == state_type.Closed) {
			Vector3 starting_pos = this.gameObject.transform.position;
			Vector3 new_pos = starting_pos + open_motion;

			StartCoroutine (Slide (this, new_pos, 1));
			action = action_type.Opening;

			foreach (int robber_id in robbers_in_proximity) {
				print ("Robber at trap: " + robber_id);
				robbers [robber_id].GetComponent<Robber> ().Scare();
			}
		} else {
			Vector3 starting_pos = this.gameObject.transform.position;
			Vector3 new_pos = starting_pos - open_motion;

			StartCoroutine (Slide (this, new_pos, 1));
			action = action_type.Closing;
		}
	}
}
