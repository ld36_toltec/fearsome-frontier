﻿using UnityEngine;
using System.Collections;

public class ResizeSprite : MonoBehaviour
{





    // Use this for initialization
    void Start()
    {

        SpriteRenderer sr = GetComponent<SpriteRenderer>();

        float worldScreenHeight = Camera.main.orthographicSize * 2;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        transform.localScale = new Vector3(
            worldScreenWidth / sr.sprite.bounds.size.x * 1.2f,
            worldScreenHeight / sr.sprite.bounds.size.y * 1.5f,
            1);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
