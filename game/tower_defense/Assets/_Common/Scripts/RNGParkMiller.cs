﻿using UnityEngine;
using System.Collections;

public class RNGParkMiller : MonoBehaviour {
	private static uint walkingNumber = (uint) (Random.value * uint.MaxValue);

	// Use this for initialization
	void Start () {
		
	}

	public static void Seed (uint value) {
		walkingNumber = value;
	}

	// http://www.avanderw.co.za/park-miller-random-number-generator-rng/
	public static float RandFloat (float minimum=0, float maximum=0) {
		if (maximum == 0)
			maximum = uint.MaxValue;

		walkingNumber = walkingNumber * 16807 % 2147483647;
		return ((walkingNumber / 0x7FFFFFFF) * maximum + minimum + 0.000000000233f);
	}

	public static int RandInt (int minimum=0, int maximum=0) {
		//! \note maximum is exclusive.
		if (maximum == 0)
			maximum = int.MaxValue;

		walkingNumber = walkingNumber * 16807 % 2147483647;
		return (int)(walkingNumber % (maximum - minimum) + minimum);
	}

	public static int RandSign () {
		int val = RandInt (0, 2);

		if (val == 0)
			return -1;
		return 1;
	}

	// Update is called once per frame
	void Update () {
	
	}
}
