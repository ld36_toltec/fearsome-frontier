﻿using UnityEngine;
using System.Collections;

public class KBCamController : MonoBehaviour {

	public float movement_speed = 2;
    public float RotateAmount = 10f;

    float zoomAmount = 0.0f;
    public float maxToClamp = 1.0f;
    public float ROTSpeed = 10.0f;

    public float maxAngle = 60f;
    public float minAngle = 10f;

	private Vector3 original_pos;
	private Quaternion original_rot;

    Plane zeroPlane;
    Vector3 rotationTarget;
    Camera curCamera;

    // Use this for initialization
    void Start () {
        zeroPlane = new Plane(Vector3.up, 0);
        curCamera = GetComponent<Camera>();
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 forward = ProjectVectorOnPlane(Vector3.up, transform.forward).normalized;
        Vector3 right = transform.TransformDirection(1, 0, 0).normalized;
        if (Input.GetKey (KeyCode.RightArrow))
			transform.position += right * movement_speed * Time.deltaTime;
		if (Input.GetKey (KeyCode.LeftArrow))
			transform.position += -right * movement_speed * Time.deltaTime;
		if (Input.GetKey (KeyCode.UpArrow))
			transform.position += forward * movement_speed * Time.deltaTime;
		if (Input.GetKey (KeyCode.DownArrow))
			transform.position += -forward * movement_speed * Time.deltaTime;

        zoomAmount += Input.GetAxis("Mouse ScrollWheel");
        zoomAmount = Mathf.Clamp(zoomAmount, -maxToClamp, maxToClamp);
        var translate = Mathf.Min(Mathf.Abs(Input.GetAxis("Mouse ScrollWheel")), maxToClamp - Mathf.Abs(zoomAmount));
        gameObject.transform.Translate(0, 0, translate * ROTSpeed * Mathf.Sign(Input.GetAxis("Mouse ScrollWheel")));
    }

	public void Reset () {
		zoomAmount = 0.0f;
		// Do we have a backup orientation to reset to?
		if (original_pos.y > 0.01f) {
			print("Resetting camera controller to " + original_pos);
			gameObject.transform.position = original_pos;
			gameObject.transform.rotation = original_rot;
		}
	}


    void LateUpdate() {
		// Backup original orientation for resetting later.
		// Acquire the backup when there's no backup yet (Y ~ 0) but the camera position has been initialized (Y > 0).
		if (original_pos.y < 0.01f && gameObject.transform.position.y > 0.01f) {
			original_pos = gameObject.transform.position;
			original_rot = gameObject.transform.rotation;
		}

        OrbitCamera();
    }

    public void OrbitCamera() {
        if (Input.GetMouseButton(1)) {
            float dist;
			Ray ray = curCamera.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane));
            if (zeroPlane.Raycast(ray, out dist)) {
                rotationTarget = ray.GetPoint(dist);
            } else {
                rotationTarget = Vector3.zero;
            }

            // Debug.DrawRay(transform.position, transform.forward, Color.green);

            float y_rotate = Input.GetAxis("Mouse X") * RotateAmount;
            float x_rotate = Input.GetAxis("Mouse Y") * RotateAmount;
            float cameraAngle = Mathf.Rad2Deg * AngleVectorPlane(transform.forward, Vector3.down);

            if ((cameraAngle < minAngle && x_rotate < 0) || (cameraAngle > maxAngle && x_rotate > 0)) {
                x_rotate = 0f;
            }

            OrbitCamera(rotationTarget, y_rotate, x_rotate);
        }
    }

    public void OrbitCamera(Vector3 target, float y_rotate, float x_rotate) {
        Vector3 angles = transform.eulerAngles;
        angles.z = 0;
        transform.eulerAngles = angles;
        transform.RotateAround(target, Vector3.up, y_rotate);
        transform.RotateAround(target, transform.TransformDirection(1, 0, 0), x_rotate);
    }

    private float AngleBetweenVector2(Vector2 vec1, Vector2 vec2) {
        Vector2 diference = vec2 - vec1;
        float sign = (vec2.y < vec1.y) ? -1.0f : 1.0f;
        return Vector2.Angle(Vector2.right, diference) * sign;
    }

    // Calculate the angle between a vector and a plane. The plane is made by a normal vector.
    // Output is in radians.
    public static float AngleVectorPlane(Vector3 vector, Vector3 normal) {
        float dot;
        float angle;

        // calculate the the dot product between the two input vectors. This gives the cosine between the two vectors
        dot = Vector3.Dot(vector, normal);
        // this is in radians
        angle = (float) Mathf.Acos(dot);

        return 1.570796326794897f - angle; // 90 degrees - angle
    }

    // Projects a vector onto a plane. The output is not normalized.
    public static Vector3 ProjectVectorOnPlane(Vector3 planeNormal, Vector3 vector) {
        return vector - (Vector3.Dot(vector, planeNormal) * planeNormal);
    }
}
