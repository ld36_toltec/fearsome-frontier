﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MummyTrap : Trap {

	// Use this for initialization
	new void Start () {
		base.Start ();

		trigger_distance = 5.0f;

		this.gameObject.transform.position += new Vector3 (0, -1, 0);

		if (state == state_type.Opened) {
			this.gameObject.transform.position += new Vector3 (0, 1, 0);
			print ("Opened at start");
		}
	}

	// Update is called once per frame
	void Update () {
		robbers = GameObject.FindGameObjectsWithTag ("Robber");
		robbers_in_proximity.Clear ();

		for (int i = 0; i < robbers.Length; i++) {
			// Robber close enough?
			if (Vector3.Distance (robbers [i].transform.position, this.transform.position) < trigger_distance) {
				robbers_in_proximity.Add (i);
			}
		}
	}

	public void Open () {
		Vector3 starting_pos = this.gameObject.transform.position;
		Vector3 new_pos = starting_pos + open_motion;

		Debug.Log ("Emerging Trap");
		StartCoroutine (Slide (this, new_pos, 4));
		action = action_type.Opening;

		Invoke ("Close", 3);
	}

	public void Close () {
		Vector3 starting_pos = this.gameObject.transform.position;
		Vector3 new_pos = starting_pos - open_motion;

		Debug.Log ("Retracting Trap");
		StartCoroutine (Slide (this, new_pos, 2));
		action = action_type.Closing;
	}

	new public void OnMouseDown () {
		if (state == state_type.Disabled) {
			Debug.Log ("Trap disabled");
		} else if (action != action_type.Idle) {
			Debug.Log ("Trap already moving.");
		} else if (state == state_type.Closed) {
			use_count++;
			if (use_count > max_use_count)
				return;

			Open ();

			foreach (int robber_id in robbers_in_proximity) {
				robbers [robber_id].GetComponent<Robber> ().Scare();
			}
		} else {
			Close ();
		}
	}
}
