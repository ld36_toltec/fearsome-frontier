﻿using UnityEngine;
using System.Collections.Generic;

// Pathfinding
// https://github.com/RonenNess/Unity-2d-pathfinding
using PathFind;

public class Robber : MonoBehaviour {

	public Transform goal;
	public float speed = 2.0f;
	public float scare_time = 10.0f;
	public float threshold = 1.0f;

	private List<PathFind.Point> path_to_follow;
	private int[] longest_route;
	private int path_step = 0;
	private float real_speed;

	private PMapLayerBase map;
	private GameController game_controller;
	Animator anim = null;
	private int current_room = -1;
	private Vector3 start_coords;

	private GameObject lantern_light = null;

	private Dictionary<int, bool> visited;
	private bool scared = false;
	private bool falling = false;
	private bool spawning = false;

	// Use this for initialization
	void Start () {
		start_coords = this.transform.position;

		visited = new Dictionary<int, bool> ();
		map = GameObject.FindGameObjectWithTag("Map").GetComponent<PMapLayerBase>();

		game_controller = (GameController) GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
		scare_time = game_controller.robber_scare_time;
		Debug.Log ("Spawned robber with a scare time of " + scare_time);

		//! \note Only the root Robber GameObject can have the Robber tag.
		//! \note All components of the Robber GameObject must be Untagged, except for RobberLantern.

		// Find lantern light.
		foreach (Transform child in transform) {
			if (child.gameObject.tag == "RobberLantern") {
				lantern_light = child.gameObject;
				break;
			}
		}

		// Find animator.
		anim = GetComponent<Animator>();

		Reset ();
	}

	public void Reset (bool forget_visited = true) {
		scared = false;
		falling = false;

		if (map != null) {
			longest_route = map.GetLongestRoute ();

			print ("Starting from " + longest_route [0]);

			// Reset the visited dict.
			if (forget_visited) {
				for (int i = 0; i < map.GetRoomCount (); i++)
					visited [i] = false;
				visited [longest_route [0]] = true;
			}

			// Find neighbors
			List<int> neighbors = map.GetNeighborRooms (longest_route [0]);
			// for (int i = 0; i < neighbors.Count; i++) {
			// 	print ("N " + neighbors [i]);
			// }

			// Pick the first neighbor.
			if (neighbors.Count > 0)
				path_to_follow = map.FindPath (longest_route [0], neighbors [0]);
		} else {
			Debug.Log ("Failed to reference an object with the Map tag.");
		}
	}

	public Vector2 GetMapPos () {
		return new Vector2 (this.transform.position.x, this.transform.position.z);
	}

	public void FollowPath (List<PathFind.Point> path) {
		path_to_follow = path;
		path_step = 0;
	}

	public void UnScare () {
		Debug.Log ("Robber no longer scared.");

		Reset (false);
		Walk ();
		PickNextTarget ();
	}

	public void SetScared () {
		scared = true;
		Invoke ("UnScare", scare_time);
	}

	public void Scare () {
		Vector2 v = new Vector2 (this.transform.position.x, this.transform.position.z);
		Debug.Log ("Robber scared!");

		anim.SetBool ("Running", true);

		path_to_follow = map.FindPath (v, longest_route [0]);
		FollowPath (path_to_follow);

		SetScared ();
	}

	public bool IsScared () {
		return scared;
	}

	public bool IsFalling () {
		return falling;
	}

	public void Stand () {
		anim.SetBool ("Moving", false);
		anim.SetBool ("Running", false);
	}

	public void Walk () {
		anim.SetBool ("Moving", true);
		anim.SetBool ("Running", false);
	}

	public void Run () {
		anim.SetBool ("Moving", true);
		anim.SetBool ("Running", true);
	}

	public void FallHome () {
		anim.SetBool ("Moving", false);

		Debug.Log ("Robber falling into a pit");

		falling = true;
		path_to_follow = null;
		path_step = 0;

		Invoke ("TeleportHome", 1);
	}

	public void TeleportHome () {
		spawning = true;
		Debug.Log ("Robber teleported into the beginning " + start_coords);
		transform.position = start_coords + new Vector3 (0, 2, 0);

		SetScared ();
	}

	// http://stackoverflow.com/questions/273313/randomize-a-listt
	private List<int> ShuffleList (List<int> list) {
		int n = list.Count;
		int k, val;

		while (n > 1) {
			n--;
			k = RNGParkMiller.RandInt (0, n + 1);
			val = list [k];
			list [k] = list [n];
			list [n] = val;
		}
		return list;
	}

	void PickNextTarget () {
		List<int> neighbors;
		int i;

		// Don't explore when scared.
		if (scared) {
			anim.SetBool ("Moving", false);
			return;
		}
		// Don't pick a target if we're outside any rooms.
		if (current_room < 0)
			return;

		// Look for another room to go to.
		neighbors = map.GetNeighborRooms (current_room);
		neighbors = ShuffleList (neighbors);

		string txt = "Visited " + current_room + "\r\n";
		for (i = 0; i < neighbors.Count; i++) {
			if (!visited [neighbors [i]]) {
				FollowPath (map.FindPath (current_room, neighbors [i]));
				txt += " N " + neighbors [i] + " Going";
				//print (txt);
				return;
			} else {
				txt += " N " + neighbors [i];
			}
		}
		//print (txt);

		// Couldn't find any unvisited neighboring rooms.
		foreach (KeyValuePair<int, bool> pair in visited) {
			// Found a room that hasn't been visited yet?
			if (!pair.Value) {
				FollowPath (map.FindPath (current_room, pair.Key));
				//print ("N " + pair.Key + " Going");
			}
		}
	}

	void MoveTo (Vector3 target) {
		float real_speed;

		target.y = transform.position.y;
		this.transform.LookAt (target);

		anim.SetBool ("Moving", true);

		// Move twice as fast when scared.
		if (scared) {
			real_speed = 2 * speed;
		} else {
			real_speed = speed;
		}

		Vector3 dir = target - transform.position;
		Vector3 dp = dir.normalized * real_speed * Time.deltaTime;

		// Avoid overshoot.
		if (dp.magnitude > dir.magnitude)
			dp = dir;

		GetComponent<CharacterController>().Move (dp);
	}

	// Update is called once per frame
	void Update () {
		if (falling) {
			transform.position += new Vector3 (0, -9.8f * Time.deltaTime, 0);
			// Have the robber fall until he hits the floor.
			if (spawning && transform.position.y <= 0.0f) {
				transform.position = new Vector3 (transform.position.x, 0, transform.position.z);
				spawning = false;
				falling = false;
			}
		} else {
			if (path_to_follow != null && path_step < path_to_follow.Count) {
				PathFind.Point target_point = path_to_follow [path_step];
				Vector3 target_v = new Vector3 (target_point.x, 0, target_point.y);

				if (Vector3.Distance (this.transform.position, target_v) <= threshold) {
					// Mark this room "visited".
					current_room = map.IsInRoom (GetMapPos ());
					if (current_room >= 0) {
						visited [current_room] = true;

						// The robber has just made the last step on this path?
						// Pick the next room, unless we've found the treasure room.
						if (path_step == path_to_follow.Count - 1 && current_room != longest_route [1])
							PickNextTarget ();
					}

					path_step++;
					return;
				}

				MoveTo (target_v);
			}

			// Shake the light, when scared.
			if (scared) {
				Vector3 offset = new Vector3 ();

				if (lantern_light != null) {
					offset.x = Mathf.Sin (Time.time * 40.0f) / 30.0f;
					offset.y = Mathf.Sin (Time.time * 40.0f + 0.2f) / 30.0f;
					offset.z = Mathf.Sin (Time.time * 40.0f + 0.3f) / 30.0f;

					lantern_light.transform.Translate(offset);
				}
			} else {
				if (lantern_light != null) {
					lantern_light.transform.position.Set (0, 0, 0);
				}
			}
		}
	}

	void OnTriggerEnter (Collider other) {
		if (other.tag == "Treasure") {
			anim.SetBool ("Moving", false);
			path_step = 0;
			path_to_follow = null;
		}
	}

	void OnDrawGizmos () {
		float y0 = 1.5f;
		Vector3 v = new Vector3 ();

		if (path_to_follow != null) {
			for (int i = 0; i < path_to_follow.Count; i++) {
				v = new Vector3 (path_to_follow [i].x, y0, path_to_follow [i].y);

				if (path_step == i)
					Gizmos.color = Color.red;
				else
					Gizmos.color = Color.yellow;

				Gizmos.DrawSphere (v, 0.2f);
			}
		}
	}

	public bool IsStillExploring () {
		return true;
	}
}
