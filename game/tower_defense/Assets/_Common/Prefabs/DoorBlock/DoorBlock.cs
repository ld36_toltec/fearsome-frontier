﻿using UnityEngine;
using System.Collections;

public class DoorBlock : MonoBehaviour {
	public enum state_type: byte
	{
		Disabled = 0,
		Closed,
		Opened
	};

	public enum action_type: byte
	{
		Idle = 0,
		Opening,
		Closing
	};

	public state_type state;
	public action_type action;

	private AudioSource audio_src;
	public AudioClip thump_low;
	public float thump_low_vol = 1;
	public AudioClip thump_med;
	public float thump_med_vol = 1;
	public AudioClip stone_slide;
	public float stone_slide_vol = 1;

	// Use this for initialization
	void Start () {
		audio_src = GetComponent <AudioSource> ();

		if (state == state_type.Opened) {
			this.gameObject.transform.position -= new Vector3 (0, -1, 0);
			print ("Opened at start");
		}
	}

	public void PlaySound(AudioClip clip, float volume, bool loop=false) {
		if (audio_src == null)
			return;

		if (loop) {
			audio_src.clip = clip;
			audio_src.volume = volume;
			audio_src.Play();
		} else {
			audio_src.PlayOneShot (clip, volume);
		}
	}

	public void StopSound() {
		if (audio_src == null)
			return;

		audio_src.Stop();
	}

	public IEnumerator Slide (DoorBlock target, Vector3 end_position, float speed) {
		PlaySound (stone_slide, stone_slide_vol, true);

		// Move the block gradually.
		while (target.transform.position != end_position) {
			target.transform.position = Vector3.MoveTowards (target.transform.position, end_position, speed * Time.deltaTime);
			yield return new WaitForEndOfFrame ();
		}

		StopSound ();

		if (target.action == action_type.Opening) {
			target.state = state_type.Opened;
			PlaySound (thump_med, thump_med_vol);
		} else if (target.action == action_type.Closing) {
			target.state = state_type.Closed;
			PlaySound (thump_low, thump_low_vol);
		}

		target.action = action_type.Idle;
	}

	void OnMouseDown () {
		if (state == state_type.Disabled) {
			print ("DoorBlock disabled");
		} else if (action != action_type.Idle) {
			print ("DoorBlock already moving.");
		} else if (state == state_type.Closed) {
			Vector3 starting_pos = this.gameObject.transform.position;
			Vector3 new_pos = starting_pos + new Vector3 (0, 1, 0);

			print ("Opening DoorBlock");
			StartCoroutine (Slide (this, new_pos, 1));
			action = action_type.Opening;
		} else {
			Vector3 starting_pos = this.gameObject.transform.position;
			Vector3 new_pos = starting_pos + new Vector3 (0, -1, 0);

			print ("Closing DoorBlock");
			StartCoroutine (Slide (this, new_pos, 1));
			action = action_type.Closing;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
